<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
spl_autoload_register(function ($class) {
    if ($class === 'Conexion' || $class==='Session') {
        return include "../../class/$class/$class.class.php";
    } else {
        include "../../class/Article/$class.class.php";
    }
});

$session = new Session();
if (!$session->validateSession('usuario')) {
    header('Location: ../../dashboard/login/login.php?message=Debes iniciar sesión&type=warningMessage');
}




?>