<?php
function validar($file)
{
    if ( ($file['img-file']['type'] !== 'image/jpeg') && ($file['img-file']['type'] !== 'image/png')) {
        return false;
    }

    if ($file['img-file']['size'] > 10000) {
        return false;
    }

    return true;
}

function upload()
{
    $file = 'img/' . basename($_FILES['uses-file']['name']);
}

if (validar($_FILES)) {
    $path = 'img/';
    $file = $path . basename($_FILES['uses-file']['name']);

    if (move_uploaded_file($_FILES['img-file']['tmp_name'], $file)) {
        header('Location: index.php');
    } else {
        header('Location: index.php?error=No cumple con las condiciones');
    }
}


?>