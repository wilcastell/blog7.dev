<?php

function validar($file){
    if (($file['img-file']['type']!== 'image/jpeg') && ($file['img-file']['type'] !== 'image/png')) {
       return false;
    }

    if ($file['img-file']['size'] > 100000) {
        return false;
    }
    return true;
}

function upload($file){
    $file = $_SERVER['DOCUMENT_ROOT']. '/img/' . basename($_FILES['img-file']['name']);
    if (!move_uploaded_file($_FILES['img-file']['tmp_name'], $file)){ 
        return false;
    }
    return $file;
}

?>