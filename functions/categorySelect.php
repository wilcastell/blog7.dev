<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
spl_autoload_register(function ($class) {
    include "../class/$class/$class.class.php";
});

function selectCategory()
{
    $categoria = new Category(new Conexion);
    return $categoria->selectToArray();
}

echo selectCategory();


?>