<?php
if (!isset($_POST['category'])) exit('Hubo un error. No se recibió la categoría!');

require_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
spl_autoload_register(function ($class) {
    include "../class/$class/$class.class.php";
});

function insertCategory(){
    $categoria = new Category(new Conexion);
    $categoria->setName($_POST['category']);
    return $categoria->insert();
}

echo insertCategory();


?>