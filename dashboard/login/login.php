<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';

spl_autoload_register(function ($class)
	{
		include "../../class/Message/$class.class.php";
	});

$message = isset($_GET['message']) && isset($_GET['type']) ? MessageFactory::createMessage($_GET['type']):false;

$message_out = $message ? $message->getMessage($_GET['message']):'';


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>lOGIN | Blog7</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="../../css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="../../css/sigin/sigin.css">
</head>
<body>

	<div class="container">
		

		<form action="validar_login.php" method="POST" role="form" class="form-signin">
			<legend>Logueate </legend>
			<p><?php echo $message_out; ?></p>
			<div class="form-group">
				<label for="">Email</label>
				<input type="email" class="form-control" name="email" placeholder="email" id="inputEmail">
			</div>

			<div class="form-group">
				<label for="">Contraseña</label>
				<input type="password" class="form-control" name="password" placeholder="Contraseña" id="inputPassword">
			</div>			
		
			<button type="submit" name="submit" class="btn btn-primary">Entrar</button>
		</form>

		
	</div>

	<script src="http://cf.dev/js/jquery.js"></script>
	<script src="http://cf.dev/js/bootstrap.min.js"></script>	
</body>
</html>