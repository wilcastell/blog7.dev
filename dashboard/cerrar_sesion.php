<?php
spl_autoload_register(function ($class) {
    include "../class/$class/$class.class.php";
});

$session = new Session();
if (!$session->validateSession('usuario')) {
    header('Location: login/login.php?message=Usuario y contraseña inválidos&type=warningMessage');
}

$session->destroySession('Location: login/login.php');

header('Location: login/login.php');

?>






