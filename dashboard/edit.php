<?php 
require 'header.php';
require 'navbar.php';
?>

    <div class="container-fluid">
      <div class="row">
        <?php require 'sidebar.php' ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>
          <h2 class="sub-header">New de Posts</h2>
          
        </div>
      </div>
     <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-7 col-md-offset-2 main">
            
            <form action="../functions/article/insert.php" method="POST" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title">
                </div>

                <div class="form-group">
                    <label for="categories">Categoría</label>
                    <select name="categories_id"  class="form-control" id="categories"></select>
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" cols="30" class="form-control" rows="10"></textarea>
                </div>

                <div class="form-group">
                    <label for="title">Image</label>
                    <input type="file" name="img-file" id="img">
                </div>
                
                <div class="form-group">
                  <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
                </div> 

            </form>

        </div>
        <div class=" col-md-3 main">
            <form action="">
                <div class="form-group">
                    <label for="new_category">New Category</label>
                    <input type="text" class="form-control" id="new_category" name="new_category" placeholder="New Category">
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary" id="submit_category" >Guardar</button>
                </div>   
            </form>
        </div>
     </div>

    </div>
</div>
<?php require 'footer.php'; ?>