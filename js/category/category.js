$(document).ready(function(){
//Actualizar selec categories
    update_select_category();
    
    $('#submit_category').on('click',function () {
        var category = $('#new_category').val();
       
        $.ajax({
            type:'POST',
            url:'../functions/categoryInsert.php',
            data:{'category':category}
        })

        .done(function (result) {
            alert(result);
            update_select_category();
        })

        .fail(function () {
            alert('hubo un error al insertar categoría :( js');        
        });

    });
});

function update_select_category() {    
    $.ajax({
        type: 'POST',
        url: '../functions/categorySelect.php'      
    })
    
    .done(function (result) {
        $('#categories').html(result);
    })

    .fail(function () {
        alert('hubo un error al actualizar las categorías :( js');
    });
}